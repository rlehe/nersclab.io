
# Jupyter at NERSC: Background Information

Curious about how Jupyter works at NERSC?
Want to make more informed decisions about how you use it?
If so, read on, this page may help you.
But you don't have to read it to make effective use of Jupyter at NERSC.

## Jupyter, JupyterLab, JupyterHub at NERSC

The terms Jupyter, JupyterLab, and JupyterHub are sometimes used
interchangeably, and this leads to confusion.
This section is intended to clarify the relationships between these terms
an put them in context at NERSC.

[Jupyter](https://jupyter.readthedocs.io/en/latest/)
is a modular,
[literate computing](https://en.wikipedia.org/wiki/Literate_programming)
software ecosystem (programming interfaces, libraries, frameworks,
applications) for managing documents called *notebooks* that contain code,
equations, visualizations, interactive widgets, text, and metadata.
Notebook files are simple, versatile JSON documents following a well-defined,
community-governed
[notebook format](https://nbformat.readthedocs.io/en/latest/).
You can edit and interact with notebooks through applications like
[Jupyter Notebook](https://jupyter-notebook.readthedocs.io/en/stable/notebook.html)
or
[JupyterLab](https://jupyterlab.readthedocs.io/en/stable/getting_started/overview.html).
Also, you can create, manipulate, execute, or transform notebooks
non-interactively by using programs that support the notebook format, like
[nbconvert](https://nbconvert.readthedocs.io/en/latest/)
or
[papermill](https://papermill.readthedocs.io/en/latest/).
You can share notebooks with others, or convert and publish them in a variety of
formats like Markdown, HTML, or even PDF.
The Jupyter notebook has become an established, versatile tool supporting
reproducible computational science, data science, and education.
Around 500 users per day, and over 1300 users per month, use Jupyter at NERSC
(May 2023).

[JupyterLab](https://jupyterlab.readthedocs.io/en/stable/getting_started/overview.html)
is a web application that lets you interact with Jupyter notebooks, but it also
provides user interface elements for editing text files, using command-line
interfaces, browsing the file system, monitoring applications, or visualizing
complex data sets.
These elements can be arranged and resized as you please, side-by-side, or
grouped together using tabs.
The goal is to provide a powerful, modular interface for interactive scientific
notebook-based workflows.
JupyterLab is a popular platform for data science and machine learning.
NERSC has supported JupyterLab as its primary user-facing Jupyter application
since its release in 2018.

[JupyterHub](https://jupyterhub.readthedocs.io/en/stable/) is another web
application for spawning, managing, and proxying instances of single-user
Jupyter notebook servers, such as JupyterLab, for multiple users.
NERSC has operated a JupyterHub instance since 2015.
While you need a NERSC account to use Jupyter at NERSC, you can use your NERSC
identity or possibly another institution's identity to authenticate yourself to
NERSC's JupyterHub at [https://jupyter.nersc.gov](https://jupyter.nersc.gov).
From there, you can spawn a JupyterLab server on a login node on a
supercomputer, or in a single-node or multi-node batch job.
Which kind of server you'll want to start up will depend on your use case,
software, and hardware requirements.
Be advised that notebook servers running in batch jobs will be charged to a
project allocation, most often your default project.
