# SIESTA

SIESTA is both a method and its computer program implementation, to
perform efficient electronic structure calculations and ab initio
molecular dynamics simulations of molecules and solids. SIESTA's
efficiency stems from the use of strictly localized basis sets and
from the implementation of linear-scaling algorithms which can be
applied to suitable systems. A very important feature of the code is
that its accuracy and cost can be tuned in a wide range, from quick
exploratory calculations to highly accurate simulations matching the
quality of other approaches, such as plane-wave and all-electron
methods.

## Availability and Supported Architectures at NERSC

SIESTA is [allowed](../../policies/software-policy/index.md) at NERSC and runs on CPUs.
SIESTA can be readily installed via [Spack](../../development/build-tools/spack.md) container.

## Application Information, Documentation, and Support

* [Manuals](https://departments.icmab.es/leem/SIESTA_MATERIAL/Docs/Manuals/manuals.html)
* [Development and distribution platform](https://launchpad.net/siesta)
    * [Questions](https://answers.launchpad.net/siesta)
    * [FAQ](https://answers.launchpad.net/siesta/+faqs)
    * [Bug reports](https://bugs.launchpad.net/siesta)

## Using SIESTA at NERSC

Siesta can be built and loaded using a Spack environment. First, load
the Spack module and create an environment:

```bash
perlmutter$ module load cpu spack
perlmutter$ mkdir <my spack directory>
perlmutter$ cd <my spack directory>
perlmutter$ spack env create -d .
perlmutter$ spack env activate <path/to/my/spack/directory>
```

Then install Siesta by:

```bash
perlmutter$ spack add siesta
perlmutter$ spack install

```

### Example run scripts

??? example "Perlmutter CPU"
    ```slurm
	#!/bin/bash
	#SBATCH --qos=regular
	#SBATCH --time=01:30:00
	#SBATCH --nodes=2
	#SBATCH --constraint=cpu

	module load cpu spack
	spack load siesta
	srun -n 64 -c 8 siesta < test.fdf > test.out
    ```

## Building Siesta from Source

Users are welcome to build their own binaries from source.

## Related Applications

* [Abinit](../abinit/index.md)
* [CP2K](../cp2k/index.md)
* [JDFTx](https://jdftx.org/)
* [Octopus](https://www.octopus-code.org/wiki/Main_Page)
* [PARSEC](http://real-space.org/)
* [RMGDFT](https://github.com/RMGDFT/rmgdft)
* [Quantum ESPRESSO](../quantum-espresso/index.md)
* [VASP](../vasp/index.md)

## User Contributed Information

!!! info "Please help us improve this page"
	Users are invited to contribute helpful information and corrections
	through our [GitLab repository](https://gitlab.com/NERSC/nersc.gitlab.io/blob/main/CONTRIBUTING.md).
