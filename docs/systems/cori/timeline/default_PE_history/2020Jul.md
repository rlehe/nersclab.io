# Programming Environment Change on Cori in July 2020

## Background

Following the scheduled maintenances on Cori during the NERSC center
power upgrade (July 9-14), we will install the new Cray Programming
Environment Software release
[CDT/20.06](https://pubs.cray.com/bundle/Released_Cray_XC_x86_Programming_Environments_20.06_June_4_2020/resource/Released_Cray_XC_x86_Programming_Environments_20.06_June_4_2020.pdf),
and retire old
[CDT/19.06](https://pubs.cray.com/bundle/Released_Cray_XC_-x86-_Programming_Environments_19.06_June_20_2019/resource/Released_Cray_XC_(x86)_Programming_Environments_19.06_June_20_2019.pdf).
There will be no software default versions change this time.

Below is the detailed list of changes after the maintenance.

## New software versions available

* cce/10.0.1
* cray-R/3.6.3.1
* cray-ccdb/4.6.4
* cray-cti/2.6.6
* cray-fftw/3.3.8.6
* cray-hdf5, cray-hdf5-parallel/1.10.6.1
* cray-libsci/20.06.1
* cray-mpich, cray-mpich-abi, cray-netcdf/4.7.3.3
* cray-netcdf-hdf5parallel/4.7.3.3
* cray-openshmemx/9.1.0
* cray-parallel-netcdf/1.12.0.1
* cray-petsc, cray-petsc-64, cray-petsc-complex, cray-petsc-complex-64/3.12.4.1
* cray-python/3.8.2.1
* cray-stat/4.5.2
* cray-tpsl, cray-tpsl-64/20.03.2
* craype/2.6.5
* craype-dl-plugin-py3/20.06.1
* craypkg-gen/1.3.10
* gcc/9.3.0
* gdb4hpc/4.6.5
* iobuf/2.0.10
* papi/6.0.0.1
* perftools-base/20.06.0
* pmi, pmi-lib/5.0.16
* valgrind4hpc/2.6.4

## Old software versions to be removed

* cce, cce-classic/9.0.0
* cray-fftw/3.3.8.3
* cray-hdf5, cray-hdf5-parallel/1.10.5.0
* cray-mpich, cray-mpich-abi, cray-shmem/7.7.8
* cray-netcdf, cray-netcdf-hdf5parallel/4.6.3.0
* cray-parallel-netcdf/1.11.1.0
* cray-python/3.6.5.7
* craype/2.6.0
* craype-dl-plugin-py2, craype-dl-plugin-py3/19.06.1
* iobuf/2.0.8
* java/jdk1.8.0_51
* modules/3.2.11.2
* papi/5.7.0.1
* perftools, perftools-base, perftools-lite/7.1.0
